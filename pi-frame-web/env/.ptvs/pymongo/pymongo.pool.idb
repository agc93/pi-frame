�}q (X   membersq}q(X   ConfigurationErrorq}q(X   kindqX   typerefqX   valueq]qX   pymongo.errorsq	X   ConfigurationErrorq
�qauX   Poolq}q(hX   typeqh}q(X   mroq]q(X   pymongo.poolqh�qX   builtinsqX   objectq�qeX   basesq]qhah}q(X   __del__q}q(hX   functionqh}q(X   docqNX   builtinq�X   staticq �X   locationq!M,K	�q"X	   overloadsq#]q$}q%(X   argsq&}q'(X   nameq(X   selfq)hhu�q*X   ret_typeq+NuauuX   _raise_wait_queue_timeoutq,}q-(hhh}q.(hNh�h �h!M&K	�q/h#]q0}q1(h&}q2(h(h)hhu�q3h+NuauuX   _get_request_stateq4}q5(hhh}q6(hNh�h �h!M"K	�q7h#]q8}q9(h&}q:(h(h)hhu�q;h+]q<(hX
   SocketInfoq=�q>hX   NoneTypeq?�q@euauuX   _return_socketqA}qB(hhh}qC(hXC   Return socket to the pool. If pool is full the socket is discarded.qDh�h �h!M�K	�qEh#]qF}qG(h&}qH(h(h)hhu}qI(h(X	   sock_infoqJh]qK(h>h@eu�qLh+NuauuX   end_requestqM}qN(hhh}qO(hNh�h �h!M�K	�qPh#]qQ}qR(h&}qS(h(h)hhu�qTh+NuauuX   connectqU}qV(hhh}qW(hX�   Connect to Mongo and return a new (connected) socket. Note that the
           pool does not keep a reference to the socket -- you must call
           return_socket() when you're done with it.qXh�h �h!M"K	�qYh#]qZ}q[(h&}q\(h(h)hhu�q]h+h>uauuX
   get_socketq^}q_(hhh}q`(hXv  Get a socket from the pool.

        Returns a :class:`SocketInfo` object wrapping a connected
        :class:`socket.socket`, and a bool saying whether the socket was from
        the pool or freshly created.

        :Parameters:
          - `force`: optional boolean, forces a connection to be returned
              without blocking, even if `max_size` has been reached.qah�h �h!M<K	�qbh#]qc}qd(h&}qe(h(h)hhu}qf(h(X   forceqghhX   boolqh�qiX   default_valueqjX   Falseqku�qlh+]qm(h>h@euauuX   __init__qn}qo(hhh}qp(hX  
        :Parameters:
          - `pair`: a (hostname, port) tuple
          - `max_size`: The maximum number of open sockets. Calls to
            `get_socket` will block if this is set, this pool has opened
            `max_size` sockets, and there are none idle. Set to `None` to
             disable.
          - `net_timeout`: timeout in seconds for operations on open connection
          - `conn_timeout`: timeout in seconds for establishing connection
          - `use_ssl`: bool, if True use an encrypted connection
          - `use_greenlets`: bool, if True then start_request() assigns a
              socket to the current greenlet - otherwise it is assigned to the
              current thread
          - `ssl_keyfile`: The private keyfile used to identify the local
            connection against mongod.  If included with the ``certfile` then
            only the ``ssl_certfile`` is needed.  Implies ``ssl=True``.
          - `ssl_certfile`: The certificate file used to identify the local
            connection against mongod. Implies ``ssl=True``.
          - `ssl_cert_reqs`: Specifies whether a certificate is required from
            the other side of the connection, and whether it will be validated
            if provided. It must be one of the three values ``ssl.CERT_NONE``
            (certificates ignored), ``ssl.CERT_OPTIONAL``
            (not required, but validated if provided), or ``ssl.CERT_REQUIRED``
            (required and validated). If the value of this parameter is not
            ``ssl.CERT_NONE``, then the ``ssl_ca_certs`` parameter must point
            to a file of CA certificates. Implies ``ssl=True``.
          - `ssl_ca_certs`: The ca_certs file contains a set of concatenated
            "certification authority" certificates, which are used to validate
            certificates passed from the other end of the connection.
            Implies ``ssl=True``.
          - `wait_queue_timeout`: (integer) How long (in seconds) a
            thread will wait for a socket from the pool if the pool has no
            free sockets.
          - `wait_queue_multiple`: (integer) Multiplied by max_pool_size to give
            the number of threads allowed to wait for a socket at one time.
          - `socket_keepalive`: (boolean) Whether to send periodic keep-alive
            packets on connected sockets. Defaults to ``False`` (do not send
            keep-alive packets).
          - `ssl_match_hostname`: If ``True`` (the default), and
            `ssl_cert_reqs` is not ``ssl.CERT_NONE``, enables hostname
            verification using the :func:`~ssl.match_hostname` function from
            python's :mod:`~ssl` module. Think very carefully before setting
            this to ``False`` as that could make your application vulnerable to
            man-in-the-middle attacks.qqh�h �h!KuK	�qrh#]qs}qt(h&(}qu(h(h)hhu}qv(h(X   pairqwhNu}qx(h(X   max_sizeqyhNu}qz(h(X   net_timeoutq{hNu}q|(h(X   conn_timeoutq}hNu}q~(h(X   use_sslqhNu}q�(h(X   use_greenletsq�hNu}q�(h(X   ssl_keyfileq�hh@hjX   Noneq�u}q�(h(X   ssl_certfileq�hh@hjh�u}q�(h(X   ssl_cert_reqsq�hh@hjh�u}q�(h(X   ssl_ca_certsq�hh@hjh�u}q�(h(X   wait_queue_timeoutq�hh@hjh�u}q�(h(X   wait_queue_multipleq�hh@hjh�u}q�(h(X   socket_keepaliveq�hhihjhku}q�(h(X   ssl_match_hostnameq�hhihjX   Trueq�utq�h+NuauuX   resetq�}q�(hhh}q�(hNh�h �h!K�K	�q�h#]q�}q�(h&}q�(h(h)hhu�q�h+NuauuX   start_requestq�}q�(hhh}q�(hNh�h �h!M�K	�q�h#]q�}q�(h&}q�(h(h)hhu�q�h+NuauuX   maybe_return_socketq�}q�(hhh}q�(hX=   Return the socket to the pool unless it's the request socket.q�h�h �h!M�K	�q�h#]q�}q�(h&}q�(h(h)hhu}q�(h(hJhNu�q�h+NuauuX   _set_request_stateq�}q�(hhh}q�(hNh�h �h!M�K	�q�h#]q�}q�(h&}q�(h(h)hhu}q�(h(hJh]q�(h>h@eu�q�h+NuauuX
   in_requestq�}q�(hhh}q�(hNh�h �h!M�K	�q�h#]q�}q�(h&}q�(h(h)hhu�q�h+hiuauuX   discard_socketq�}q�(hhh}q�(hX$   Close and discard the active socket.q�h�h �h!M�K	�q�h#]q�}q�(h&}q�(h(h)hhu}q�(h(hJhNu�q�h+NuauuX   _checkq�}q�(hhh}q�(hXx  This side-effecty function checks if this pool has been reset since
        the last time this socket was used, or if the socket has been closed by
        some external network error, and if so, attempts to create a new socket.
        If this connection attempt fails we reset the pool and reraise the
        error.

        Checking sockets lets us avoid seeing *some*
        :class:`~pymongo.errors.AutoReconnect` exceptions on server
        hiccups, etc. We only do this if it's been > 1 second since
        the last socket checkout, to keep performance reasonable - we
        can't avoid AutoReconnects completely anyway.q�h�h �h!M�K	�q�h#]q�}q�(h&}q�(h(h)hhu}q�(h(hJh]q�(h>h@eu�q�h+]q�(h>h@euauuX   create_connectionq�}q�(hhh}q�(hXx   Connect and return a socket object.

        This is a modified version of create_connection from
        CPython >=2.6.q�h�h �h!K�K	�q�h#]q�}q�(h&}q�(h(h)hhu�q�h+]q�(h@hX   _socketq�X   socketq�q�h�h�q�euauuX   _check_interval_secondsq�}q�(hX   dataq�h}q�hhX   intq�q�suX   socketsq�}q�(hh�h}q�hhX   setq�q�suX   lockq�}q�(hh�h}q�hX   _dummy_threadq�X   LockTypeq�q�suX   pool_idq�}q�(hh�h}q�hh�suX   pidq�}q�(hh�h}q�hh�suhw}q�(hh�h}q�hNsuX   max_sizeq�}q�(hh�h}q hNsuh{}r  (hh�h}r  hNsuh}}r  (hh�h}r  hNsuh�}r  (hh�h}r  hh@suh�}r  (hh�h}r  hh@suh�}r	  (hh�h}r
  hhisuh}r  (hh�h}r  hNsuh�}r  (hh�h}r  hh@suh�}r  (hh�h}r  hh@suh�}r  (hX   multipler  h}r  h}r  (hh�h}r  hh@su}r  (hh�h}r  hh�su�r  suh�}r  (hh�h}r  hh@suh�}r  (hh�h}r  hhisuX   _tid_to_sockr  }r  (hh�h}r  hhX   dictr   �r!  suX   _identr"  }r#  (hj  h}r$  h}r%  (hh�h}r&  hX   pymongo.thread_utilr'  X   GreenletIdentr(  �r)  su}r*  (hh�h}r+  hj'  X   ThreadIdentr,  �r-  su�r.  suX   _request_counterr/  }r0  (hh�h}r1  hj'  X   Counterr2  �r3  suX   _socket_semaphorer4  }r5  (hj  h}r6  h(}r7  (hh�h}r8  hj'  X    MaxWaitersBoundedSemaphoreGeventr9  �r:  su}r;  (hh�h}r<  hj'  X   DummySemaphorer=  �r>  su}r?  (hh�h}r@  hj'  X    MaxWaitersBoundedSemaphoreThreadrA  �rB  su}rC  (hh�h}rD  hj'  X   BoundedSemaphorerE  �rF  sutrG  suuhNh�h!KtK�rH  uuX   timerI  }rJ  (hX	   modulerefrK  hX   timerL  X    rM  �rN  uX   match_hostnamerO  }rP  (hj  h}rQ  h}rR  (hX   funcrefrS  h}rT  X	   func_namerU  X)   pymongo.ssl_match_hostname.match_hostnamerV  su}rW  (hjS  h}rX  jU  X   ssl.match_hostnamerY  su�rZ  suX
   NO_REQUESTr[  }r\  (hh�h}r]  hh@suX   sslr^  }r_  (hjK  hX   sslr`  jM  �ra  uX   Requestrb  }rc  (hhh}rd  (h]re  (hjb  �rf  heh]rg  hah}rh  (hn}ri  (hhh}rj  (hNh�h �h!M>K	�rk  h#]rl  }rm  (h&}rn  (h(h)hjf  u}ro  (h(X
   connectionrp  h]rq  (X   pymongo.replica_set_connectionrr  X   ReplicaSetConnectionrs  �rt  X    pymongo.mongo_replica_set_clientru  X   MongoReplicaSetClientrv  �rw  X   pymongo.mongo_clientrx  X   MongoClientry  �rz  eu�r{  h+NuauuX   __exit__r|  }r}  (hhh}r~  (hNh�h �h!MGK	�r  h#]r�  }r�  (h&(}r�  (h(h)hjf  u}r�  (h(X   exc_typer�  hNu}r�  (h(X   exc_valr�  hNu}r�  (h(X   exc_tbr�  hNutr�  h+hiuauuX   endr�  }r�  (hhh}r�  (hNh�h �h!MAK	�r�  h#]r�  }r�  (h&}r�  (h(h)hjf  u�r�  h+NuauuX	   __enter__r�  }r�  (hhh}r�  (hNh�h �h!MDK	�r�  h#]r�  }r�  (h&}r�  (h(h)hjf  u�r�  h+jf  uauujp  }r�  (hj  h}r�  h}r�  (hh�h}r�  hjt  su}r�  (hh�h}r�  hjw  su}r�  (hh�h}r�  hjz  su�r�  suuhX�   
    A context manager returned by :meth:`start_request`, so you can do
    `with client.start_request(): do_something()` in Python 2.5+.r�  h�h!M9K�r�  uuX   _closedr�  }r�  (hhh}r�  (hX?   Return True if we know socket has been closed, False otherwise.r�  h�h �h!K,K�r�  h#]r�  }r�  (h&}r�  (h(X   sockr�  h]r�  (hh@j`  X	   SSLSocketr�  �r�  eu�r�  h+hiuauuX   selectr�  }r�  (hjS  h}r�  jU  X   select.selectr�  suX   ConnectionFailurer�  }r�  (hhh]r�  h	X   ConnectionFailurer�  �r�  auX   thread_utilr�  }r�  (hjK  hj'  jM  �r�  uX   sysr�  }r�  (hjK  hX   sysr�  jM  �r�  uX   weakrefr�  }r�  (hjK  hX   weakrefr�  jM  �r�  uX   HAS_SSLr�  }r�  (hj  h}r�  h}r�  (hh�h}r�  hhisu}r�  (hh�h}r�  hhisu�r�  suX   osr�  }r�  (hjK  hX   osr�  jM  �r�  uX	   threadingr�  }r�  (hjK  hX	   threadingr�  jM  �r�  uX   socketr�  }r�  (hjK  hh�jM  �r�  uX   NO_SOCKET_YETr�  }r�  (hh�h}r�  hNsuh=}r�  (hhh}r�  (h]r�  (h>heh]r�  hah}r�  (X   closer�  }r�  (hhh}r�  (hNh�h �h!KIK	�r�  h#]r�  }r�  (h&}r�  (h(h)hh>u�r�  h+NuauuX   __ne__r�  }r�  (hhh}r�  (hNh�h �h!KdK	�r�  h#]r�  }r�  (h&}r�  (h(h)hh>u}r�  (h(X   otherr�  hNu�r�  h+Nuauuhn}r�  (hhh}r�  (hNh�h �h!K:K	�r�  h#]r�  }r�  (h&(}r�  (h(h)hh>u}r�  (h(j�  h]r�  (h@hj�  h�h�eu}r�  (h(h�hh�u}r�  (h(X   hostr�  hh@hjh�utr�  h+NuauuX   __eq__r�  }r   (hhh}r  (hNh�h �h!K_K	�r  h#]r  }r  (h&}r  (h(h)hh>u}r  (h(j�  hNu�r  h+hiuauuX   max_wire_versionr  }r	  (hX   propertyr
  h}r  (hNhh@h!K[K	�r  uuX   min_wire_versionr  }r  (hj
  h}r  (hNhh@h!KVK	�r  uuX   __repr__r  }r  (hhh}r  (hNh�h �h!KjK	�r  h#]r  }r  (h&}r  (h(h)hh>u�r  h+hX   strr  �r  uauuX   __hash__r  }r  (hhh}r  (hNh�h �h!KgK	�r  h#]r  }r   (h&}r!  (h(h)hh>u�r"  h+h�uauuX   set_wire_version_ranger#  }r$  (hhh}r%  (hNh�h �h!KQK	�r&  h#]r'  }r(  (h&}r)  (h(h)hh>u}r*  (h(j  hNu}r+  (h(j  hNu�r,  h+Nuauuj�  }r-  (hj  h}r.  h}r/  (hh�h}r0  hj�  su}r1  (hh�h}r2  hh@su}r3  (hh�h}r4  hhsu�r5  suj�  }r6  (hh�h}r7  hh@suX   authsetr8  }r9  (hh�h}r:  hh�suX   closedr;  }r<  (hj  h}r=  h}r>  (hh�h}r?  hhisu}r@  (hh�h}rA  hhisu�rB  suX   last_checkoutrC  }rD  (hh�h}rE  hhX   floatrF  �rG  suX   forcedrH  }rI  (hj  h}rJ  h}rK  (hh�h}rL  hhisu}rM  (hh�h}rN  hhisu�rO  suX   _min_wire_versionrP  }rQ  (hh�h}rR  hh@suX   _max_wire_versionrS  }rT  (hh�h}rU  hh@suh�}rV  (hh�h}rW  hh�suuhX!   Store a socket with some metadatarX  h�h!K7K�rY  uuuhjM  X   childrenrZ  ]r[  X   filenamer\  XU   C:\Users\alist_000\Source\pi-frame\pi-frame-web\env\Lib\site-packages\pymongo\pool.pyr]  u.